// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// SetInterval выполняет действия постояяно, а сеттаймаут один раз

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Как только весь код будет прочитан, setTimeout с нулевой задержкой будет выполняться первым.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Чтобы остановить выполнение функциии



const btnStopAnimation = document.createElement('button')
btnStopAnimation.textContent = "Припинити"
const btnContinueAnimation = document.createElement('button')
btnContinueAnimation.textContent = 'Відновіти показ'
const img = document.querySelectorAll('.image-to-show')
let counter = 0
let imgPool = [...img]

let timer = null


const animate = () => {
    if (timer) return
    document.body.prepend(btnStopAnimation)
    btnStopAnimation.after(btnContinueAnimation)
    timer = setInterval(() => {
        if (counter === imgPool.length) {
            counter = 0;
        }
        const current = imgPool[counter];
        const prevElIndex = counter - 1

        if (counter === 0) {
            imgPool[imgPool.length-1].classList.add('images-visibility')
        } else {
           img[prevElIndex].classList.add('images-visibility');
        }
        current.classList.remove('images-visibility');

        counter++
    }, 3000);

}
animate()
btnStopAnimation.addEventListener('click', () => {
    clearInterval(timer)
    timer = null
})
btnContinueAnimation.addEventListener('click', animate)






// function hiddenImg() {
//     img.forEach((el, index) => {
//         setInterval(() => {
//             console.log(index)
//             el.classList.remove('images-visibility')
//         }, 3000 + (index * 1000))
//     })
// }
// hiddenImg()

